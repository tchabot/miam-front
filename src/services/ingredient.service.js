import axios from "axios";
import { TokenService } from "./token.service";

const IngredientService = {
  findAll: async function() {
    let url = process.env.VUE_APP_API_BASE_URL + "/ingredients";

    return await axios.get(url, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },

  findOne: async function(id) {
    let url = process.env.VUE_APP_API_BASE_URL + "/ingredients/" + id;

    return await axios.get(url, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },

  update: async function(id, data) {
    let url = process.env.VUE_APP_API_BASE_URL + "/ingredients/" + id;

    return await axios.put(url, data, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },

  create: async function(data) {
    let url = process.env.VUE_APP_API_BASE_URL + "/ingredients";

    return await axios.post(url, data, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },

  delete: async function(id) {
    let url = process.env.VUE_APP_API_BASE_URL + "/ingredients/" + id;

    return await axios.delete(url, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },
};

export { IngredientService };
