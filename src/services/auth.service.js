import { TokenService } from "../services/token.service";
import axios from "axios";

const AuthService = {
  register: async function(email, password) {
    let success = await axios
      .post(process.env.VUE_APP_API_BASE_URL + "/register", {
        email: email,
        password: password,
      })
      .then(() => {
        return true;
      })
      .catch((error) => {
        console.log(error);
        return false;
      });

    return success;
  },

  login: async function(email, password) {
    let newToken = await axios
      .post(process.env.VUE_APP_API_BASE_URL + "/login", {
        email: email,
        password: password,
      })
      .then((response) => {
        TokenService.saveToken(response.data.access_token);
        return response.data.access_token;
      })
      .catch((error) => {
        console.log(error);
        return null;
      });

    return newToken;
  },

  logout: async function() {
    TokenService.removeToken();
    return true;
  },
};

export { AuthService };
