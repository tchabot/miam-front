import axios from "axios";
import { TokenService } from "./token.service";
import { UserService } from "./user.service";
import { UserListService } from "./user-list.service";
import { IngredientService } from "./ingredient.service";
import { UnitService } from "./unit.service";

const ShopListService = {
  findAll: async function() {
    let url = process.env.VUE_APP_API_BASE_URL + "/shop-list";

    return await axios.get(url, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },

  findByUserAndIngredient: async function(userId, ingredientId) {
    let url = `${process.env.VUE_APP_API_BASE_URL}/shop-list?userId=${userId}&ingredientId=${ingredientId}`;

    return await axios.get(url, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },

  findOne: async function(id) {
    let url = process.env.VUE_APP_API_BASE_URL + "/shop-list/" + id;

    return await axios.get(url, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },

  update: async function(id, data) {
    let url = process.env.VUE_APP_API_BASE_URL + "/shop-list/" + id;

    return await axios.put(url, data, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },

  create: async function(data) {
    let url = process.env.VUE_APP_API_BASE_URL + "/shop-list";
    let user = await UserService.getCurrentUser();
    user = user.data;

    if (data.ingredientId) {
      let result = await IngredientService.findOne(data.ingredientId);
      data.ingredient = result.data;
    }

    if (data.unitId) {
      let result = await UnitService.findOne(data.unitId);
      data.unit = result.data;
    }

    let existingShopItem = await this.findByUserAndIngredient(
      user.id,
      data.ingredient.id
    );

    //If ingredient alreay in shop list so just add the quantity
    if (existingShopItem.data !== "") {
      if (data.quantity) {
        existingShopItem.data.quantity += data.quantity;
      }

      this.update(existingShopItem.data.id, existingShopItem.data);
    } else {
      let userList = null;

      if (!data.userList || data.userList === null) {
        userList = await UserListService.findByUserId(user.id);
        //TODO choisir une liste par défaut (prendre grande surface)
        userList = userList.data[0];
      }

      let newData = {
        checked: false,
        userList: data.userList ?? userList,
        ingredient: data.ingredient,
        quantity: data.quantity,
        unit: data.unit,
      };

      return await axios.post(url, newData, {
        headers: { Authorization: "Bearer " + TokenService.getToken() },
      });
    }
  },

  delete: async function(id) {
    let url = process.env.VUE_APP_API_BASE_URL + "/shop-list/" + id;

    return await axios.delete(url, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },
};

export { ShopListService };
