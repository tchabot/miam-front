const ToolsService = {
  formatIngredient: (cook) => {
    let result = cook.ingredient.name;

    if (cook.quantity) {
      result += " (" + cook.quantity;
    }

    if (!cook.unit) {
      return (result += ")");
    } else {
      return (result += " " + cook.unit.name + ")");
    }
  },
};

export { ToolsService };
