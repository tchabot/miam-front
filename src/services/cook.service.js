import axios from "axios";
import { TokenService } from "./token.service";

const CookService = {
  findAll: async function() {
    let url = process.env.VUE_APP_API_BASE_URL + "/cooks";

    return await axios.get(url, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },

  findByRecipe: async function(recipeId) {
    let url = `${process.env.VUE_APP_API_BASE_URL}/cooks?recipeId=${recipeId}`;

    return await axios.get(url, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },

  update: async function(id, data) {
    let url = process.env.VUE_APP_API_BASE_URL + "/cooks/" + id;

    return await axios.put(url, data, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },

  create: async function(data) {
    let url = process.env.VUE_APP_API_BASE_URL + "/cooks";

    return await axios.post(url, data, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },

  delete: async function(id) {
    let url = process.env.VUE_APP_API_BASE_URL + "/cooks/" + id;

    return await axios.delete(url, {
      headers: { Authorization: "Bearer " + TokenService.getToken() },
    });
  },
};

export { CookService };
