import Vue from "vue";
import Vuex from "vuex";
import router from "../../router/index";
import { TokenService } from "../../services/token.service";
import { AuthService } from "../../services/auth.service";

Vue.use(Vuex);

const user = {
  namespaced: true,

  state: {
    token: TokenService.getToken(),
    authenticationError: false,
    registerError: false,
  },

  mutations: {
    SET_TOKEN(state, token) {
      state.token = token;
    },
    SET_AUTHENTICATION_ERROR(state, value) {
      state.authenticationError = value;
    },
    SET_REGISTER_ERROR(state, value) {
      state.registerError = value;
    },
  },

  getters: {
    loggedIn: (state) => {
      return state.accessToken ? true : false;
    },
    authenticationError: (state) => {
      return state.authenticationError;
    },
    registerError: (state) => {
      return state.registerError;
    },
  },

  actions: {
    async login(context, data) {
      let idToken = await AuthService.login(data.email, data.password);

      if (!idToken) {
        context.commit("SET_AUTHENTICATION_ERROR", true);
        return false;
      } else {
        context.commit("SET_TOKEN", idToken);
        return true;
      }
    },

    async logout(context) {
      let success = AuthService.logout();

      if (success) {
        context.commit("SET_TOKEN", null);
        router.push("/login");
      }
    },

    async register(context, data) {
      let success = await AuthService.register(data.email, data.password);

      if (success) {
        router.push("/login");
      } else {
        context.commit("SET_REGISTER_ERROR", true);
      }
    },
  },
};

export default user;
