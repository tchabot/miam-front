import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";
import Register from "../views/Register.vue";
import Recipes from "../views/Recipes.vue";
import Units from "../views/Units.vue";
import Categories from "../views/Categories.vue";
import Ingredients from "../views/Ingredients..vue";
import RecipeAction from "../views/RecipeAction.vue";
import RecipeDetails from "../views/RecipeDetails.vue";
import ShopList from "../views/ShopList.vue";
import UserList from "../views/UserList.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "root",
    component: Login,
  },
  {
    path: "/login",
    name: "login",
    component: Login,
  },
  {
    path: "/register",
    name: "register",
    component: Register,
  },
  {
    path: "/recipes",
    name: "recipes",
    component: Recipes,
  },
  {
    path: "/recipes/action/:id?",
    name: "recipes-action",
    component: RecipeAction,
  },
  {
    path: "/recipes/:id",
    name: "recipe-details",
    component: RecipeDetails,
  },
  {
    path: "/units",
    name: "units",
    component: Units,
  },
  {
    path: "/categories",
    name: "categories",
    component: Categories,
  },
  {
    path: "/ingredients",
    name: "ingredients",
    component: Ingredients,
  },
  {
    path: "/shop-list",
    name: "shop-list",
    component: ShopList,
  },
  {
    path: "/lists",
    name: "lists",
    component: UserList,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
