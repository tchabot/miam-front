import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import { TokenService } from "../src/services/token.service";

Vue.config.productionTip = false;

const publicRoutes = ["login", "register"];

router.beforeEach((to, from, next) => {
  if (!publicRoutes.includes(to.name) && !TokenService.getToken()) {
    next({ name: "login" });
  }

  next();
});

Vue.directive("longpress", {
  bind: function(el, binding) {
    let pressTimer = null;

    // Define funtion handlers
    // Create timeout ( run function after 1s )
    let start = (e) => {
      if (e.type === "click" && e.button !== 0) {
        return;
      }

      if (pressTimer === null) {
        pressTimer = setTimeout(() => {
          handler();
        }, 500);
      }
    };

    let cancel = () => {
      if (pressTimer !== null) {
        clearTimeout(pressTimer);
        pressTimer = null;
      }
    };

    const handler = (e) => {
      //Execute method that is passed to the directive
      binding.value(e);
    };

    // Add Event listeners
    el.addEventListener("mousedown", start);
    el.addEventListener("touchstart", start);
    // Cancel timeouts if this events happen
    el.addEventListener("click", cancel);
    el.addEventListener("mouseout", cancel);
    el.addEventListener("touchend", cancel);
    el.addEventListener("touchcancel", cancel);
  },
});

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
